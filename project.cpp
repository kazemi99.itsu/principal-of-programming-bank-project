#include<stdio.h>
#include<stdlib.h>
#include<string.h>


//variz,bardasht,enteghal

FILE* bank;

struct Person
{
    char name[20];
    char last_name[20];
    long int national_code;
    //int primary_code;
};

struct employee
{
    Person per;
    //int person_key;
    //int employee_key;
    //position;
};

struct customer
{
    Person per;
    //int person_key;
};

struct random_person
{

};


struct accounts
{
    long int account_number;
    //type;
    int value;
};

void add_person()
{
    Person pr;
    printf("Please enter his/her name:\n");
    scanf("%s",pr.name);

    printf("Please enter his/her last name:\n");
    scanf("%s",pr.last_name);

    printf("Please enter his/her national code:\n");
    scanf("%ld",&pr.national_code);

    fseek(bank,0,SEEK_END);
	fwrite(&pr,sizeof(Person),1,bank);
}

void print_person(Person pr)
{
	printf("%s %s with national code %ld found\n",pr.name,pr.last_name,pr.national_code);
}

int find_name(Person* pper)
{
    char name1[20];
	puts("Enter his/her name:");
	scanf("%s",name1);
	Person pr;
	fseek(bank,0,SEEK_SET);
	while(fread(&pr,sizeof(Person),1,bank))
    {
        if(strcmp(name1,pr.name)>0 || strcmp(name1,pr.name)<0)
            continue;
        *pper=pr;
        return 1;
	}
	return 0;
}

int find_last_name(Person* pper)
{
    char name1[20];
	puts("Enter his/her last name:");
	scanf("%s",name1);
	Person pr;
	fseek(bank,0,SEEK_SET);
	while(fread(&pr,sizeof(Person),1,bank))
    {
        if(strcmp(name1,pr.last_name)>0 || strcmp(name1,pr.last_name)<0)
            continue;
        *pper=pr;
        return 1;
	}
	return 0;
}

int find_ncode(Person* pper)
{
    long int ncode1;
	puts("Enter his/her national code:");
	scanf("%ld",&ncode1);
	Person pr;
	fseek(bank,0,SEEK_SET);
	while(fread(&pr,sizeof(Person),1,bank))
    {
        if( ncode1!=pr.national_code )
            continue;
        *pper=pr;
        return 1;
	}
	return 0;
}

void search_ncode()
{
    Person per;
    int res=find_ncode(&per);
    if(!res)
        printf("person cannot be found!\n");
    else
        print_person(per);
    system("pause");
}

void search_name()
{
    Person per;
	int res=find_name(&per);
	if(!res)
		printf("Person cannot be found!\n");
	else
		print_person(per);
	system("pause");
}

void search_last_name()
{
    Person per;
	int res=find_last_name(&per);
	if(!res)
		printf("Person cannot be found!\n");
	else
		print_person(per);
	system("pause");
}

void edit_name_person(){
	Person pr;
	int res=find_name(&pr);
	if(!res)
		printf("Person cannot be found!\n");
	else
	{
		printf("Please enter the new name:\n");
		char c;
		scanf("%c%[^\n]%c",&c,pr.name,&c);
		fseek(bank,-sizeof(Person),SEEK_CUR);
		fwrite(&pr,sizeof(Person),1,bank);
	}
}

void edit_lastname_person(){
	Person pr;
	int res=find_last_name(&pr);
	if(!res)
		printf("Person cannot be found!\n");
	else
	{
		printf("Please enter the new last name:\n");
		char c;
		scanf("%c%[^\n]%c",&c,pr.last_name,&c);
		fseek(bank,-sizeof(Person),SEEK_CUR);
		fwrite(&pr,sizeof(Person),1,bank);
	}
}

int menu()
{
    system("cls");
    printf("0.EXIT\n");
    printf("1.Person\n");
    printf("2.Employee\n");
    printf("3.Customer\n");
    printf("4.person's account list\n");
    printf("5.person's account transactions\n");
    printf("6.Persons list\n");
    printf("7.Search\n");
    printf("8.Add account\n");
    
    int n;
    scanf("%d",&n);
    return n;
}

void act(int choice)
{
    switch(choice)
    {

    case 1:
        printf("1.Add person\n");
        printf("2.Edit person\n");
        printf("3.Delete person\n");
        int n1;
        scanf("%d",&n1);
        switch(n1)
        {
        case 1:
            add_person();
            break;
        case 2:
            printf("1.edit name\n");
            printf("2.edit last name\n");
            int n5;
            scanf("%d",&n5);
            switch(n5)
            {
            case 1:
                edit_name_person();
                break;
            case 2:
                edit_lastname_person();
                break;
            }
            break;
        case 3:
            //delete_person();
            break;
        }
        break;

    case 2:
        printf("1.Add employee\n");
        printf("2.Edit employee\n");
        printf("3.Delete employee\n");
        printf("4.Login\n");
        int n2;
        scanf("%d",&n2);
        switch(n2)
        {
        case 1:
            //add_employee();
            break;
        case 2:
            //edit_employee();
            break;
        case 3:
            //delete_employee();
            break;
        case 4:
            //login_employee();
            break;
        }
        break;

    case 3:
        printf("1.Add customer\n");
        printf("2.Edit customer\n");
        printf("3.Delete customer\n");
        printf("4.Login\n");
        int n3;
        scanf("%d",&n3);
        switch(n3)
        {
        case 1:
            //add_customer();
            break;
        case 2:
            //edit_customer();
            break;
        case 3:
            //delete_customer();
            break;
        case 4:
            //login_customer();
            break;
        }
        break;

    case 4:
        //print_person(persons list);
        int n6;
        scanf("%d",&n6);
        switch(n6)

        //print(struct account list);
        break;
    
    case 5:
        //print(struct transaction list);
        break;
    
    case 6:
        //print_person(persons list);
        break;

    case 7:
        printf("1.Search with name\n");
        printf("2.Search with last name\n");
        printf("3.Search with national code\n");
        int n4;
        scanf("%d",&n4);
        switch(n4)
        {
        case 1:
            search_name();
            break;
        case 2:
            search_last_name();
            break;
        case 3:
            search_ncode();
            break;
        }
        break;
    
    case 8:
        //
        break;
    }
}

// FILE *open(char name[], char mode[]){
//     FILE *payload;
//     payload = fopen (name, mode);
//     return payload;
// } 


int main()
{
    // bank = open("matin.txt", "w+");
    // fputs("This is Guru99 Tutorial on fputs,", bank);

    while(1)
    {
        int choice=menu();
        if(choice==0)
            return 0;
        else
            act(choice);
    }
}
